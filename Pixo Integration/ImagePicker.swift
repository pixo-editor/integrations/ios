import UIKit
import SwiftUI

struct ImagePicker: UIViewControllerRepresentable {
    
    var sourceType: UIImagePickerController.SourceType = .photoLibrary
    
    @Binding var selectedImage: UIImage
    @Binding var base64Image: String
    @Environment(\.presentationMode) private var presentationMode

    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
        
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.sourceType = sourceType
        imagePicker.delegate = context.coordinator
        
        return imagePicker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<ImagePicker>) {
        
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    final class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        var parent: ImagePicker
        
        init(_ parent: ImagePicker) {
            self.parent = parent
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                let imageData:Data = image.pngData()!
                let strBase64:String = imageData.base64EncodedString()
                
                parent.base64Image = """
                    <html>
                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    </head>
                    <body>
                        <script src="https://pixoeditor.com/editor/scripts/bridge.m.js"></script>
                        <script>
                        function sendMessage(message) {
                            if (
                                window.webkit
                                    && window.webkit.messageHandlers
                                    && window.webkit.messageHandlers.jsHandler
                            ) {
                                window.webkit.messageHandlers.jsHandler.postMessage(
                                    message
                                );
                            }
                        }
                        var img = '\(strBase64)';
                        new Pixo.Bridge({
                            type: 'modal',
                            apikey: '26uqyj79pdgk',
                            theme: 'ios',
                            onSave: image => sendMessage(image.toBase64()),
                            onCancel: () => sendMessage(''),
                        }).edit('data:image/png;base64,' + img)
                        </script>
                    </body>
                    </html>
"""
            }
            
            parent.presentationMode.wrappedValue.dismiss()
        }
    }
}

