import SwiftUI
import WebKit

struct ContentView: View {
    
    @State private var isPhotoLibraryShown = false
    @State private var image = UIImage()
    @State private var base64Image = ""
    
    var body: some View {
        if base64Image != "" {
            WebView(text: $base64Image, image: $image)
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        } else {
            VStack {
                Image(uiImage: self.image)
                    .resizable()
                    .scaledToFill()
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .edgesIgnoringSafeArea(.all)
                
                Button(action: {
                    self.isPhotoLibraryShown = true
                }) {
                    HStack {
                        Image(systemName: "photo")
                            .font(.system(size: 20))
                            
                        Text("Choose a photo")
                            .font(.headline)
                    }
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 50)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(20)
                    .padding(.horizontal)
                }
            }
            .sheet(isPresented: $isPhotoLibraryShown) {
                ImagePicker(sourceType: .photoLibrary, selectedImage: self.$image, base64Image: self.$base64Image)
            }
        }
    }
}

struct WebView: UIViewRepresentable {
    @Binding var text: String
    @Binding var image: UIImage
   
    func makeUIView(context: Context) -> WKWebView {
        let uiView = WKWebView()
        let contentController = ContentController(self)
        uiView.configuration.userContentController.add(contentController, name: "jsHandler")
        return uiView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.loadHTMLString(text, baseURL: nil)
    }
    
    class ContentController: NSObject, WKScriptMessageHandler {
        var parent: WebView
        
        init(_ parent: WebView) {
            self.parent = parent
        }
        
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            if message.name == "jsHandler"{
                let body = message.body as? String ?? ""
                if body != "" {
                    let imageData = Data(base64Encoded: body)
                    parent.image = UIImage(data: imageData!)!
                }
                parent.text = ""
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
